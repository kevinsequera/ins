<!--Template del Header-->
<?php 
	session_start();
	include('header.php');
	/** @author: Victor Poderoso, Cristina García, Kevin Sequera
	* @version : versió 1.8;
	*/
?>
<!-- Aquesta pàgina ens serà el menú inicia, o sigui, la primera de totes.-->
<article class="container cos-pagina">
	<section class="row">
		<h2>Benvinguts a <span class="empresa">IAM Motors</span></h2>
	</section>
	<!--Començem demanant la matricula, que comprovarem després si ja esta enregistrada o no.-->
	<section class="row seccio-central"> 
		<form method="GET" action="comprova.php">
			<input id="matricula" class="matricula" type="text" name="matricula" autocomplete="off" placeholder="Introduir la matrícula">
			<input class="matricula" type="submit" name="submit" value="Enviar" >
		</form>
	</section>
</article>
<!--Template Footer, està en tots els documents.-->
<?php include('footer.php');?>