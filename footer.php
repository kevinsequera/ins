<footer id="peu" class="peu container-fluid">
	<section class="container">
		<h4>Informació</h4>
	</section>
	<section class="info container">
		<section class="column copyright">
			<h5>Copyright ©, 2017</h5>
			<span>Kevin Sequera Ríos</span> <br>
			<span>Victor Poderoso de Moya</span> <br>
			<span>Cristina García Rodríguez</span> <br>
		</section>
		<section class="column contact">
			<h5>Contacte</h5>
			Telf. <span> 932 033 642</span><br> Adreça electrònica: <a href="mailto:info@iammotors.cat">info@iammotors.cat</a><br>
			Aquesta pàgina esta feta per alumnes del institut <a hred="http://www.institutausiasmarch.cat/">Ausias March</a> BCN
		</section>
	</section>
</footer>

</body>

</html>